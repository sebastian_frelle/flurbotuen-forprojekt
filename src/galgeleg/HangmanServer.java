package galgeleg;

import java.rmi.Naming;
import javax.xml.ws.Endpoint;




public class HangmanServer {

	public static void main(String[] arg) throws Exception
	{
		// RMI:
//		java.rmi.registry.LocateRegistry.createRegistry(1099); // start i server-JVM
//
//		IHangman hangman = new Galgelogik();
//		Naming.rebind("rmi://localhost/hangman", hangman);
//		System.out.println("Hangmanserver registreret.");

		//SOAP:
		
		IHangman hangman = new Galgelogik();
		Endpoint.publish("http://[::]:9963/hangman", hangman);
		System.out.println("Hangman publiceret over SOAP");
		
	}
}
