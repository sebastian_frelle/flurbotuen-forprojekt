package galgeleg;

import java.net.URL;
import java.rmi.Naming;
import java.util.Scanner;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class BenytGalgelegModServer {
	public static void main(String[] arg) throws Exception {

		//RMI:
		//IHangman hangman = (IHangman) Naming.lookup("rmi://localhost/hangman");
		
		//Soap:
		URL url = new URL("http://ubuntu4.javabog.dk:9963/hangman?wsdl");
		QName qname = new QName("http://galgeleg/", "GalgelogikService");
		Service service = Service.create(url, qname);
		IHangman hangman = service.getPort(IHangman.class);

		Scanner scanner = new Scanner(System.in);

		System.out.println("Velkommen til galgeleg");

		boolean loginkorrekt;
		do {

			System.out.println("Brugernavn:");
			String username = scanner.nextLine();

			System.out.println("Adgangskode");
			String password = scanner.nextLine();

			loginkorrekt = hangman.login(username, password);

			if (loginkorrekt) {
				System.out.println("Korrekt");
			} else {
				System.out.println("Forkert brugernavn eller adgangskode, prøv igen");
			}
		} while (!loginkorrekt);

		boolean forsatspil;
		do {
			while (!hangman.erSpilletSlut()) {

				System.out.println("");
				System.out.println(hangman.getSynligtOrd());
				System.out.println("Gæt et bogstav");

				String gæt = scanner.nextLine().substring(0, 1);
				hangman.gætBogstav(gæt);
				System.out.println("Brugte bogstaver " + hangman.getBrugteBogstaver());
				System.out.println("Antal liv tilbage: " + (7-hangman.getAntalForkerteBogstaver()));

				if (hangman.erSpilletVundet()) {
					System.out.println("Tillykke du har vundet spillet");
				} else if (hangman.erSpilletTabt()) {
					System.out.println("Du tabte, ordet var " + hangman.getOrdet());
				}
			}
			System.out.println("Klik ja for at prøve igen");
			
			forsatspil = scanner.nextLine().equals("ja");
			
			if(forsatspil) { 
				System.out.println("Nyt spil starter");
				hangman.nulstil();
			}
			
		} while (forsatspil);
		
		System.out.println("På gensyn");
		hangman.nulstil();
		scanner.close();
	}
}

