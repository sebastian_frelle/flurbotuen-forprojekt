package client;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class WordRestClient {
    public static List<String> getWords() {
        String url = "http://ubuntu4.javabog.dk:54163/api/words";
        System.out.print("Getting word list from " + url + " ... ");

        Client client = ClientBuilder.newClient();
        Response res = client.target(url)
                .request(MediaType.APPLICATION_JSON)
                .get();

        System.out.println("Done");

        String resContents = res.readEntity(String.class);
        JsonArray jsonArray = new JsonParser().parse(resContents).getAsJsonArray();

        return (jsonArray.size() > 0) ? mapJsonWordsToList(jsonArray) : getListOfWords();
    }

    private static List<String> mapJsonWordsToList(JsonArray jsonWords) {
        List<String> words = new ArrayList<>();
        for (JsonElement e : jsonWords) {
            JsonObject jsonObj = e.getAsJsonObject();
            words.add(jsonObj.get("name").getAsString());
        }

        return words;
    }

    private static List<String> getListOfWords() {
        List<String> words = new ArrayList<>();
        words.add("bil");
        words.add("computer");
        words.add("programmering");
        words.add("motorvej");
        words.add("busrute");
        words.add("gangsti");
        words.add("skovsnegl");
        words.add("solsort");

        return words;
    }


}
